# parapipe

## About

`parapipe` is a CLI tool that utilizes YAML configs to run parallel pipelines in Docker or on the host for automating CI/CD and streamlining infrastructure operations.

`parapipe` is a powerful command-line application that allows users to easily run one or more pipelines in parallel. The tool reads a config path of layered YAML files and feeds it into the pipelines as tasks, which can be executed in Docker containers or on the running host.

The inspiration for `parapipe` came from the need for a tool that would make it easy to run multiple pipelines simultaneously, while still allowing for flexible and customizable configuration options. With `parapipe`, users can define their pipelines and tasks in clear and concise YAML files, making it easy to understand and modify the configuration.

Whether you are a developer looking to automate your CI/CD pipeline or a DevOps engineer looking to streamline your infrastructure, `parapipe` is a good tool for the job. With its parallel execution capabilities and easy-to-use YAML configuration, `parapipe` allows you to focus on what really matters - getting your tasks done quickly and efficiently.

Overall, `parapipe` is a versatile, user-friendly and efficient tool that can help you improve your workflows and increase productivity. With its powerful capabilities and simple configurable structure, it is sure to be a valuable addition to any developer or DevOps team's toolkit.
