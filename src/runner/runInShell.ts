import { Task, TaskResult } from "../pipeline/types";
import execSh from "exec-sh";

export default async (task: Task): Promise<TaskResult> => {
  return execSh
    .promise(task.command, { cwd: task.workingDir })
    .then((std) => {
      return {
        task,
        status: {
          code: 0,
          msg: std.stdout,
        },
      };
    })
    .catch((error) => {
      return {
        task,
        status: {
          code: error.code,
          msg: error.message,
        },
        error,
      };
    });
};
