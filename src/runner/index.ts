import chalk from "chalk";
import path from "path";
import { ParapipeConfig } from "../config/types";
import logger from "../logger";
import { Task, TaskResult } from "../pipeline/types";
import runInDocker from "./runInDocker";
import runInShell from "./runInShell";

const SKIPPED_CODE = 86;

export default (task: Task, configs: ParapipeConfig) => {
  const projectName = path.basename(configs.repository.workingDir);
  logger.log("spinner", `${projectName} -> ${task.name}: `, `Loading task...`);

  if (task.when) {
    logger.log(
      "spinner",
      chalk.black.bgCyan(`${projectName} -> ${task.name}: `),
      chalk.cyan(`skipped`)
    );

    const matches = /([^==|!=]+)(!=|==)(.*)/.exec(task.when);

    if (
      matches &&
      !compare(matches[1].trim(), matches[2].trim(), matches[3].trim())
    ) {
      return Promise.resolve({
        task,
        status: {
          code: SKIPPED_CODE,
        },
      } as TaskResult);
    }
  }

  logger.log("spinner", `${projectName} -> ${task.name}: `, "running");

  if (task.image) {
    task.workingDir = process.cwd();
    return runInDocker({
      ...task,
      workingDir: `/opt/repository/${path.relative(
        task.workingDir,
        configs.repository.workingDir
      )}`,
      volumes: {
        "/opt/repository": {},
      },
      binds: [`${task.workingDir}:/opt/repository`],
    }).then(taskResultCallback(projectName));
  }
  task.workingDir = path.resolve(configs.repository.workingDir);
  return runInShell(task).then(taskResultCallback(projectName));
};

const taskResultCallback = (projectName: string) => (result: TaskResult) => {
  projectName = projectName ? `${projectName} -> ` : "";
  logger.log(
    "spinner",
    chalk.bgGreen.black(`${projectName}${result.task.name}: `),
    "done"
  );
  return result;
};

type scalarType = string | number | boolean;

const compare = (left: scalarType, operator: string, right: scalarType) =>
  ((
    {
      "==": (left, right) => left === right,
      "!=": (left, right) => left !== right,
    } as Record<string, (l: scalarType, r: scalarType) => boolean>
  )[operator](left, right));
