import { Task, TaskResult } from "../pipeline/types";
import Docker from "dockerode";
import logger from "../logger";
import runInShell from "./runInShell";

export default async (task: Task): Promise<TaskResult> => {
  const docker = new Docker();

  logger.debug("Running `runInDocker`", task);

  if (!task.image) {
    throw {
      message: "Cannot run in docker without image.",
      task,
    };
  }

  logger.info(`Pulling docker image ${task.image}.`);
  await runInShell({
    name: "docker-pull-task-image",
    command: `docker pull ${task.image}`,
    workingDir: process.cwd(),
  }).then(() => logger.info(`Done pulling image.`));

  return docker
    .run(
      task.image,
      [task.shell ?? "/bin/sh", "-c", task.command],
      process.stdout,
      {
        WorkingDir: task.workingDir,
        Volumes: task.volumes ?? [],
        Env: Object.entries(process.env).map(([k, v]) => `${k}=${v}`),
        Hostconfig: {
          Binds: task.binds ?? {},
        },
      }
    )
    .then(([output, container]) => {
      container.remove();
      return [output, container];
    })
    .then(([output, container]) => {
      return {
        task,
        status: {
          code: output.StatusCode,
          msg: ""
        },
        error: output.Error,
        details: [[output, container]],
      } as TaskResult;
    });
};
