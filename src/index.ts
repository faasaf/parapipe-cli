import * as dotenv from "dotenv";
import { initProgram } from "./command";
import { program } from "./program";

dotenv.config();
initProgram(program);
program.parse();
