import { Command } from "commander";
import configYaml from "config-yaml";
import fs from "fs";
import lodash from "lodash";
import commands from "./commands";
import { ParapipeCommand, ParapipeCommandOption } from "./commands/types";
import { configs, expandValues, loadConfig } from "./config";
import { RepositoryConfig, RunConfig } from "./config/types";
import { repositoryPath } from "./fs";

const loadRunConfig = (): Partial<RunConfig> =>
  (configs.runconfig = lodash.merge(
    configs.runconfig,
    fs.existsSync(`${repositoryPath}/.parapiperc`)
      ? expandValues(configYaml(`${repositoryPath}/.parapiperc`))
      : {}
  ));

const loadRepositoryConfig = (): Partial<RepositoryConfig> =>
  (configs.repository = loadConfig(configs.runconfig, {}));

export const globalOptions: ParapipeCommandOption[] = [];

export const initProgram = (program: Command) => {
  loadRunConfig();
  loadRepositoryConfig();
  addCommands(program);
  addGlobalOptions(program);
};

const addOption = (program: any) => (option: any) => {
  program.option(option.flags, option.help, option.default);
};

const addCommand = (
  program: Command,
  name: string,
  command: ParapipeCommand
) => {
  const program_command = program.command(name);
  program_command.description(command.description);
  program_command.action((options) => command.action(configs, options));

  command.arguments?.forEach((arg: any) => {
    program_command.argument(
      `[${arg.name}]`,
      arg.help,
      arg.default ?? undefined
    );
  });

  command.options?.forEach(addOption(program_command));
};

const addGlobalOptions = (program: any) => {
  globalOptions.forEach(addOption(program));
  globalOptions.forEach((option) => {
    program.commands.forEach((c: Command) => addOption(c)(option));
  });
};

const addCommands = (program: Command) => {
  Object.values(commands).forEach((command) => {
    addCommand(program, command.name, command);
    return;
  });
};
