import configYaml from "config-yaml";
import { createHash } from "crypto";
import { existsSync, readFileSync } from "fs";
import lodash from "lodash";
import { parse, stringify } from "yaml";
import hostinfo from "./hostinfo";
import { RepositoryConfig, RunConfig } from "./types";

const expressionTest = /("?\(\) => .*"?)|(\$\{[^}]+\})/;

const recurseEphemeralConfig = (
  ephemeralConfig: Record<string, any>,
  parent: any
) => {
  if (!ephemeralConfig) {
    return;
  }
  Object.entries(ephemeralConfig).forEach(([key, entry]) => {
    if (typeof entry === "object") {
      ephemeralConfig[key] = recurseEphemeralConfig(entry, parent);
      return;
    }

    const entryMatches = expressionTest.exec(entry);
    if (!entryMatches) {
      return;
    }

    const path = entryMatches[1] ?? entryMatches[2];
    const replaceValue = getReplaceValueFor(path, parent);
    ephemeralConfig[key] = entry.replace(path, replaceValue);
  });

  return ephemeralConfig;
};

export const expandValues = (config: Record<string, any>) => {
  if (!config) {
    return {};
  }

  let configString = stringify(config);
  const maxIterations = Math.pow(Object.values(config).length, 2);
  let iteration = 0;

  while (expressionTest.test(configString) && maxIterations > iteration) {
    ++iteration;
    const parsedConfig = parse(configString);
    const ephemeralConfig = recurseEphemeralConfig(parsedConfig, parsedConfig);
    configString = stringify(ephemeralConfig);
  }

  return parse(configString);
};

export const contextFunctions = {
  hash: (type: string, input: string) =>
    createHash(type).update(input).digest("hex"),
  base64: (input: string, decode = false) =>
    decode
      ? Buffer.from(input, "base64").toString("utf8")
      : Buffer.from(input, "utf8").toString("base64"),
};

const evalConfigFunction = (
  configFunction: string,
  document: Record<string, any>
) => {
  return function () {
    return eval(configFunction);
  }.call({
    config: document,
    ...contextFunctions,
  });
};

export const getReplaceValueFor = (
  path: string,
  document: Record<string, any>
) => {
  const functionRegExp = /^"?(\(\) => .*)"?$/;
  const functionMatches = functionRegExp.exec(path);
  if (functionMatches) {
    const result = evalConfigFunction(functionMatches[1], document)();
    return result;
  }
  const propPath = path.replace("${", "").replace("}", "");
  return lodash.get(document, propPath);
};

export const loadConfig = (
  runconfig: RunConfig,
  baseConfig: Record<string, any>
): RepositoryConfig =>
  expandValues(
    lodash.merge(
      { ...baseConfig, ...runconfig },
      existsSync(runconfig.configPath)
        ? configYaml(readFileSync(runconfig.configPath))
        : {}
    )
  );

export const configs: {
  runconfig: RunConfig;
  repository: RepositoryConfig;
} = {
  runconfig: {
    pipeline: "**/pipeline.yaml",
    configPath: ".parapipe",
    env: process.env,
    hostinfo: hostinfo(),
  },
  repository: {
    workingDir: process.cwd(),
  },
};
