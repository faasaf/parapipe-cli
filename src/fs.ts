import { dirname } from "path";
import Promise from "bluebird";
import fsRaw from "fs";

const fs = Promise.promisifyAll(fsRaw);

export const repositoryPath = process.cwd();

export const deleteFromRepository = (path: string) =>
  fs.rmAsync(`${repositoryPath}/${path}`, {});

export const writeToRepository = (path: string, contents: string) =>
  fs
    .mkdirAsync(dirname(`${repositoryPath}/${path}`))
    .then(() => fs.writeFileAsync(`${repositoryPath}/${path}`, contents));

export const readFromRepository = (path: string) =>
  fs.readFileAsync(`${repositoryPath}/${path}`);

export const fileExistsInRepository = (path: string) =>
  fs.existsAsync(`${repositoryPath}/${path}`);

export const readFile = (path: string) => fs.readFileAsync(path);
