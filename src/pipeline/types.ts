export type TaskHandler = (
  task: Task,
  pipeline: Pipeline
) => Promise<TaskResult>;

export interface Pipeline {
  stages: string[];
  pipeline: Record<string, Task[]>;
}

export interface Task {
  name: string;
  command: string;
  depends?: string[];
  when?: string;
  image?: string;
  binds?: Record<string, any>;
  shell?: string;
  volumes?: Record<string, any>;
  workingDir?: string;
}

export enum TaskStatus {
  SUCCESS = 0,
  SKIPPED = 86
}

export type TaskResult = {
  task: Task;
  status: {
    code: TaskStatus|number;
    msg: string;
  };
  error?: any;
  details?: any;
};

export type PipelineTaskRegister = Record<
  string,
  Promise<TaskResult> | Promise<null>
>;
