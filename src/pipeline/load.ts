import { globby } from "globby";

export default (glob: string) => {
  return globby(glob, {
    onlyFiles: true,
  });
};
