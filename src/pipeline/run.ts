import {
  Pipeline,
  Task,
  TaskHandler,
  TaskResult
} from "./types";

export default async (
  pipeline: Pipeline,
  taskHandler: TaskHandler
) => {
  const promises = await buildRegister(pipeline, taskHandler);

  return Promise.all(Object.values(promises).filter((v) => !!v));
};

/**
 * Gets input stages and pipelines and builds a task registry filled with
 * promises to full-fill using a taskHandler.
 *
 * Input examples:
 *
 * stages:
 *   - build
 *   - test
 *   - publish
 *
 * pipeline:
 *   build:
 *    - name: build
 *      cmd: echo "build"
 *   test:
 *    - name: test
 *      cmd: echo "test"
 *   publish:
 *    - name: publish
 *      cmd: echo "publish"
 *      depends:
 *        - build:build
 *
 * const taskHandler = (task, runnerVars) => {
 *   console.log(task, runnerVars);
 * }
 *
 */
const buildRegister = async (pipeline: Pipeline, taskHandler: TaskHandler) => {
  const taskRegister: Record<string, TaskHandler | TaskHandler[]> = {};
  const promises: Record<string, Promise<TaskResult|null>> = {};
  let lastStageName = "";

  const stages = pipeline.stages;

  for (const stageName of stages.filter(() => true)) {
    const tasks = pipeline.pipeline[stageName].filter(() => true);

    tasks.forEach(async (task) => {
      const taskName = `${stageName}:${task.name}`;
      let handler = taskHandler;
      if (task.depends) {
        handler = deferTask(
          taskHandler,
          task.depends
            .filter((dep) => !!taskRegister[dep])
            .map((dep) => taskRegister[dep])
            .flat() as TaskHandler[]
        );
      }

      if (lastStageName && !task.depends) {
        handler = deferTask(
          taskHandler,
          taskRegister[lastStageName] as TaskHandler[]
        );
      }

      promises[taskName] = handler(task, pipeline);
      (taskRegister[stageName] as TaskHandler[]).push(
        taskRegister[taskName] as TaskHandler
      );
    });

    promises[stageName] = (async () => {
      await Promise.all(taskRegister[stageName] as TaskHandler[]);
      return null;
    })();

    lastStageName = stageName;
  }

  return promises;
};

const deferTask =
  (taskHandler: TaskHandler, queue: TaskHandler[]): TaskHandler =>
  async (task: Task, pipeconfig) => {
    await Promise.all(queue);
    return taskHandler(task, pipeconfig);
  };
