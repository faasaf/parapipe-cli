import { ChalkLogger } from "@guanghechen/chalk-logger";
import chalkTable from "chalk-table";
import ora from "ora";
import { Logger } from "./types";

const chalkLogger = new ChalkLogger({
  name: "faasaf-cli",
});

const spinner = ora("");

export const debugHandler = async (logger: Logger, ...args: any[]) => {
  chalkLogger.debug(JSON.stringify(args, undefined, 2));
};

export const infoHandler = async (logger: Logger, message: string) => {
  chalkLogger.info(message);
};

export const tableHandler = async (logger: Logger, table: any) => {
  const ctable = chalkTable(table.options, table.data);
  console.log("");
  console.log("");
  console.log(table.title);
  console.log("");
  console.log(ctable);
};

export const errorHandler = async (
  logger: Logger,
  message: string,
  data: any
) => {
  chalkLogger.error(message);
  logger.debug(data);
};

export const spinnerStartHandler = (
  logger: Logger,
  preText: string,
  text: string
) => {
  logger.log("spinner", preText, text);
  spinner.start();
};

export const spinnerHandler = (
  logger: Logger,
  preText: string,
  text: string
) => {
  spinner.text = text;
  spinner.prefixText = preText;
};

export const spinnerStopHandler = (
  logger: Logger,
  preText: string,
  text: string
) => {
  logger.log("spinner", preText, text);
  spinner.stop();
};
