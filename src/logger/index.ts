import {
  debugHandler,
  errorHandler,
  infoHandler,
  spinnerHandler,
  spinnerStartHandler,
  spinnerStopHandler,
  tableHandler,
} from "./stdout.js";
import * as dotenv from "dotenv";
import { LogHandler } from "./types.js";

dotenv.config();

interface LoggerInstance<T> {
  handlers: Record<string, Record<string, T>>;
  setHandler(type: string, key: string, handler: T): void;
  debug: (...args: any[]) => void;
  error: (...args: any[]) => void;
  info: (...args: any[]) => void;
  log: (...args: any[]) => void;
}

const createLogger = (debug: boolean): LoggerInstance<LogHandler> => ({
  handlers: {},
  setHandler(type, key, handler) {
    if (!this.handlers[type]) {
      this.handlers[type] = {};
    }

    this.handlers[type][key] = handler;
  },

  log(type, ...args) {
    if (!this.handlers[type]) {
      return;
    }

    const meLogger = this;

    Promise.all(
      (
        Object.values(this.handlers[type]).filter(
          (h) => h instanceof Function
        ) as Function[]
      ).map((h) => h(meLogger, ...args))
    );
  },
  info(...args) {
    this.log("info", ...args);
  },
  debug(...args) {
    if (debug) {
      this.log("debug", ...args);
    }
  },
  error(...args) {
    this.log("error", ...args);
  },
});

const logger = createLogger(!!process.env.PARAPIPE_CLI_DEBUG);

logger.setHandler("debug", "stdout", debugHandler);
logger.setHandler("table", "stdout", tableHandler);
logger.setHandler("error", "stdout", errorHandler);
logger.setHandler("info", "stdout", infoHandler);
logger.setHandler("spinner:start", "stdout", spinnerStartHandler);
logger.setHandler("spinner", "stdout", spinnerHandler);
logger.setHandler("spinner:stop", "stdout", spinnerStopHandler);

export default logger;
