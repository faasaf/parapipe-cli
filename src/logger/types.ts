export interface Logger {
  debug: LogHandler;
  info: LogHandler;
  log: (...args: any[]) => void;
}

export type LogHandler = (logger: Logger, ...args: any[]) => void;
