import { RepositoryConfig, RunConfig } from "../config/types";

export interface ParapipeCommandOption {
  flags: string;
  help: string;
  default?: string;
}

export interface ParapipeCommandArgument {}

export interface ParapipeCommand {
  name: string;
  description: string;
  options: ParapipeCommandOption[];
  action: (
    configs: { repository: RepositoryConfig; runconfig: RunConfig },
    options: Record<string, any>
  ) => Promise<void>;
  arguments: ParapipeCommandArgument[];
}
