import chalk from "chalk";
import path from "path";
import { parse } from "yaml";
import { expandValues } from "../config";
import { ParapipeConfig } from "../config/types";
import { readFile } from "../fs";
import logger from "../logger";
import pipeline from "../pipeline";
import {
  Pipeline,
  Task,
  TaskHandler,
  TaskResult,
  TaskStatus,
} from "../pipeline/types";
import { program } from "../program";
import runner from "../runner";
import { ParapipeCommand, ParapipeCommandOption } from "./types";

const options: ParapipeCommandOption[] = [
  {
    flags: "-c, --pipeconfig <config>",
    default: ".pipeconfig",
    help: "path for pipeconfig files.",
  },
  {
    flags: "-p, --pipeline <pipeline>",
    default: "**/*/pipeline.yaml",
    help: "pipeline glob.",
  },
  {
    flags: "-t, --tags <tags>",
    help: "run tasks with only these tags.",
  },
  {
    flags: "-x, --exclude-tags <exclude-tags>",
    help: "run tasks excluding listed tags.",
  },
];

const action = async (
  configs: ParapipeConfig,
  options: Record<string, any>
) => {
  const pipelines = await pipeline.load(options.pipeline);
  let errors: TaskResult[] = [];
  const results = await runPipelines(configs, options, pipelines).then(
    (pipelineResult) => {
      Object.entries(pipelineResult).forEach(([_, tasks]) => {
        const errorTasks = (tasks
          .filter((t) => !!t)
          .filter((t) => t && t.status.code > 0) ?? []) as TaskResult[];
        errors.concat(errorTasks);

        if (errorTasks.length) {
          logger.error(
            `Pipeline finished with "${errorTasks.length}" error(s).`,
            errorTasks
          );
        }
      });
      return pipelineResult;
    }
  );

  logResults(results);

  if (errors.length) {
    const msg = `Pipelines finished with "${errors.length}" error(s).`;
    return program.error(chalk.red.bold(msg));
  }

  logger.info(`Pipelines finished successfully.`);
};

export const command: ParapipeCommand = {
  name: "run",
  description: "run pipeline(s)",
  options,
  action,
  arguments: [],
};

const logResults = (taskResult: Record<string, (TaskResult | null)[]>) => {
  Object.entries(taskResult).forEach(async ([pipeline, tasks]) => {
    if (!tasks.length) {
      logger.info(`Pipeline ${pipeline} did not have any tasks.`);
      return;
    }

    const tasksTableData = (tasks.filter((r) => !!r) as TaskResult[]).map(
      (result) => ({
        name: result.task.name,
        command: result.task.command,
        result:
          result.status.code === TaskStatus.SKIPPED
            ? chalk.blue(`skipped`)
            : result.status.code === TaskStatus.SUCCESS
            ? chalk.green(`done`)
            : chalk.red(`error`),
      })
    );

    logger.log("table", {
      title: chalk.bold.bgWhite.black(`Results for ${pipeline}:`),
      options: {
        leftPad: 1,
        columns: [
          { field: "name", name: "Name" },
          { field: "command", name: "Command" },
          { field: "result", name: "Result" },
        ],
      },
      data: tasksTableData,
    });
  });
};

const runPipelines = async (
  configs: ParapipeConfig,
  options: Record<string, any>,
  pipelines: string[]
) => {
  const results: Record<string, (TaskResult | null)[]> = {};

  logger.log("spinner:start", `⏳`, `loading pipelines...`);

  for (const pipelineConfigPath of pipelines) {
    const pipelineBasename = path.basename(path.dirname(pipelineConfigPath));
    logger.log("spinner", `⏳`, `loading pipeline ${pipelineBasename}...`);

    const pipelineConfig = await processPipelineConfig(
      pipelineConfigPath,
      configs
    );

    logger.log("spinner", `⏳`, `running pipeline ${pipelineBasename}...`);

    results[pipelineConfigPath] = await pipeline
      .run(pipelineConfig, taskHandler(configs))
      .then((tasks) => tasks.filter((t) => !!t));

    logger.log("spinner", `⌛`, `done running pipeline ${pipelineBasename}...`);
  }
  logger.log("spinner:stop", `⌛`, `done running pipelines`);

  return results;
};

const taskHandler =
  (configs: ParapipeConfig): TaskHandler =>
  (task: Task): Promise<TaskResult> =>
    runner(task, configs);

const processPipelineConfig = async (
  pipeline: string,
  configs: ParapipeConfig
) => {
  const pipelineConfig = await readFile(pipeline).then((document) =>
    expandValues({ pipeline: parse(document.toString()), configs })
  );

  return pipelineConfig.pipeline as Pipeline;
};
