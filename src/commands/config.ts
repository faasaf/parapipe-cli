import { ParapipeCommand } from "./types";
import util from "util";

export default {
  name: "config",
  description: "Show the current configuration",
  options: [
    {
      flags: "-r, --raw",
      help: "Show the raw configuration JSON (to be used with other tools like jq)",
    },
  ],
  action: async (configs, options) => {
    if (options.raw) {
      console.log(JSON.stringify(configs));
      return;
    }

    console.log(
      util.inspect(configs, { showHidden: false, depth: null, colors: true })
    );
  },
  arguments: [],
} as ParapipeCommand;
