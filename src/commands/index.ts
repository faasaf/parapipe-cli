import { command as run } from "./run";

import config from "./config";
import { ParapipeCommand } from "./types";

export default [config, run] as ParapipeCommand[];
